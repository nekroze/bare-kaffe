defmodule Bkaffe do
  use Application
  require Logger

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    children = [
      worker(Kaffe.Consumer, []),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [restart: :temporary, strategy: :one_for_one, name: Bkaffe.Supervisor]
    Bkaffe.runtime_configure()
    Logger.debug "Initializing consumer supervisor"
    Supervisor.start_link(children, opts)
  end

  def runtime_configure() do
    static_kaffe_opts = [
      heroku_kafka_env: true,
      async_mesage_ack: false,
      start_with_earliest_message: true,
    ]
    Application.put_env(:kaffe, :consumer,
                        [
                          topics: Confex.get_env(:bkaffe, :topics),
                          consumer_group: Confex.get_env(:bkaffe, :group),
                          message_handler: Confex.get_env(:bkaffe, :handler),
                        ]
                        ++ static_kaffe_opts)
    Application.put_env(:logger, :level, Confex.get_env(:bkaffe, :logging))
    :ok
  end
end
