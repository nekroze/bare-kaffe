defmodule Bkaffe.Consumer do
  require Logger

  def handle_message(message) do
    Logger.debug fn() -> message |> inspect() end

    message[:value]
    |> Poison.decode!()
    |> reactor()

    :ok
  end

  defp reactor(message) do
    Logger.debug fn() -> message |> inspect() end
    :ok
  end
end
