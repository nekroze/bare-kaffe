defmodule Bkaffe.Supervisor do
  use Supervisor
  require Logger

  def init(token) do
    children = [
      worker(Kaffe.Consumer, []),
      worker(Bkaffe.Chatter, [token])
    ]

    # For other strategies and supported options see:
    # http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    opts = [
      restart: :permanent,
      strategy: :one_for_all
    ]
    Logger.debug "Starting Bkaffe.Supervisor"
    Supervisor.init(
      children,
      opts
    )
  end

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, opts, name: __MODULE__)
  end

end
