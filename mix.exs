defmodule Bkaffe.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bkaffe,
      version: "0.0.1",
      elixir: "~> 1.2",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps(),
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [
     applications: [
       :poison,
       :kaffe,
       :confex,
     ],
     extra_applications: [
       :logger,
     ],
     mod: {Bkaffe, []}
    ]
  end

  defp deps() do
    [
      {:distillery, "~> 1.4", runtime: false},
      {:poison, "~> 3.1"},
      {:confex, "~> 3.2.3"},
      {:kaffe, "~> 1.3.0"},
    ]
  end

  defp aliases() do
    [
      "develop": [
        "init",
        "run --no-halt",
      ],
      "init": [
        "local.hex --force",
        "local.rebar --force",
        "deps.get",
      ],
    ]
  end
end
