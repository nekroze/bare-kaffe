# Bkaffe

Consumes Apache Kafka messages.

## Installation

### Production

A production image can be built using docker:
```
 $ docker build -t bkaffe .
```

### Development

To bring up a testing or development version run either of the following:

#### Dev

Code changes are immediately available in the container:
```
 $ docker-compose up
```

#### Production Like

Code changes are baked in at build time:
```
 $ docker-compose -f docker-compose.prod.yml -f docker-compose.yml up
```

To incorporate new code changes run this command before the previous one:
```
 $ docker-compose -f docker-compose.prod.yml -f docker-compose.yml build
```
