# Import all plugins from `rel/plugins`
# They can then be used by adding `plugin MyPlugin` to
# either an environment, or release definition, where
# `MyPlugin` is the name of the plugin module.
Path.join(["rel", "plugins", "*.exs"])
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: Mix.env()

# For a full list of config options for both releases
# and environments, visit https://hexdocs.pm/distillery/configuration.html

# You may define one or more environments in this file,
# an environment's settings will override those of a release
# when building in that environment, this combination of release
# and environment configuration is called a profile

environment :dev do
  # It is recommended that you build with MIX_ENV=prod and pass
  # the --env flag to Distillery explicitly if you want to use
  # dev mode.
  set dev_mode: true
  set include_erts: false
  set cookie: :"X0os2}d!a,@?U)6aijA/uJ?f|;so9tvv^98;c),[;,Tj[$rr&}eAO;(]U^K}!gP["
end

environment :prod do
  set include_erts: true
  set include_src: false
  set cookie: :"Tece8yfenhK;OpX?%BXK?1^@{Sc,lsswB[v1m.0NEf;&9lO(ioI`CcW&9$l6Xg7i"
end

release :bkaffe do
  set version: current_version(:bkaffe)
  set applications: [
    :runtime_tools
  ]
end
